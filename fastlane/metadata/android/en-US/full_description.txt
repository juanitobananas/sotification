<i>Sotification</i> is an extremely simple app that shows a notification with the screen-on time for the current day.

<i>Sotification</i> is open source and released under the GPLv3 license[1]. Check out the code if you like[2].

<b>Required Android Permissions</b>

▸ REQUEST_IGNORE_BATTERY_OPTIMIZATIONS to make sure it keeps running in the background.
▸ RECEIVE_BOOT_COMPLETED to automatically startup on boot if activated.

<b>Get it</b>
You can download it for free in IzzyOnDroid's F-Droid repo[3]

<b>Translations</b>

Translations are always welcome! :)

The app is available for translation as two projects on Transifex[4]

[1] https://www.gnu.org/licenses/gpl-3.0.en.html
[2] https://gitlab.com/juanitobananas/sotification
[3] https://apt.izzysoft.de/packages/com.jarsilio.android.sotification/
[4] https://www.transifex.com/juanitobananas/sotification and https://www.transifex.com/juanitobananas/libcommon
