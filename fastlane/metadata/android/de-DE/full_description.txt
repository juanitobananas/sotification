<i>Sotification</i> ist eine extrem einfache App, die eine Benachrichtigung mit dem heutigen Screen-On-Time zeigt.

<i>Sotification</i> ist Open Source und wird unter der GPLv3-Lizenz veröffentlicht[1]. Schauen Sie sich den Code an, wenn Sie möchten[2].

<b>Erforderliche Android-Berechtigungen</b>

• REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, um sicherzustellen, dass es im Hintergrund läuft.
• RECEIVE_BOOT_COMPLETED, automatisch beim Booten zu starten, wenn aktiviert.

<b>Laden Sie sie herunter</b>
Du kannst die App kostenlos in IzzyOnDroids F-Droid-Repo herunterladen[3]

<b>Übersetzungen</b>

Übersetzungen sind immer willkommen! :)

Die App steht als zwei Projekte auf Transifex zur Übersetzung zur Verfügung[4]

[1] https://www.gnu.org/licenses/gpl-3.0.en.html
[2] https://gitlab.com/juanitobananas/sotification
[3] https://apt.izzysoft.de/packages/com.jarsilio.android.sotification/
[4] https://www.transifex.com/juanitobananas/sotification und https://www.transifex.com/juanitobananas/libcommon