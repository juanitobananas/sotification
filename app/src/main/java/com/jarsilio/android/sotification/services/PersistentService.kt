package com.jarsilio.android.sotification.services

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.jarsilio.android.common.extensions.isOreoOrNewer
import com.jarsilio.android.sotification.MainActivity
import com.jarsilio.android.sotification.R
import com.jarsilio.android.sotification.extensions.isIgnoringBatteryOptimizations
import com.jarsilio.android.sotification.extensions.isScreenOn
import com.jarsilio.android.sotification.prefs.Prefs
import com.jarsilio.android.sotification.requireApplicationContext
import timber.log.Timber
import java.lang.IllegalArgumentException

class PersistentService : Service() {

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun start() {
        registerScreenReceiver()
    }

    private fun stop() {
        unregisterScreenReceiver()
    }

    override fun onDestroy() {
        stop()
    }

    override fun onCreate() {
        super.onCreate()
        start()
    }

    private fun registerScreenReceiver() {
        val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        registerReceiver(ScreenReceiver, filter)

        if (isScreenOn) {
            // Start directly if screen is on
            ScreenReceiver.scheduleTimer()
            if (ScreenReceiver.lastScreenOn == 0L) {
                // Also assume screen just turned on if the last timestamp is unknown (a.k.a)
                ScreenReceiver.lastScreenOn = System.currentTimeMillis()
            }
        }
    }

    private fun unregisterScreenReceiver() {
        ScreenReceiver.unscheduleTimer()
        try {
            unregisterReceiver(ScreenReceiver)
        } catch (e: IllegalArgumentException) {
            Timber.d("Failed to unregister ScreenReceiver (${e.message}). Probably was never registered. This should be okay.")
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (shouldStartForegroundService(this)) {
            startForegroundService()
        }

        return START_STICKY
    }

    private fun startForegroundService() {
        Timber.d("Starting ForegroundService")
        startForeground(FOREGROUND_ID, NotificationHandler.persistentNotification.build())
    }

    companion object {

        const val FOREGROUND_ID = 10001

        fun startService(context: Context) {
            val prefs = Prefs
            if (prefs.isEnabled) {
                Timber.i("Starting Sotification")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                    shouldStartForegroundService(context)
                ) {
                    Timber.d("Starting service with context.startForegroundService (Android >= Oreo and battery optimization on)")
                    context.startForegroundService(Intent(context, PersistentService::class.java))
                } else {
                    Timber.d("Starting service with context.startService (Android < Oreo or battery optimization off)")
                    context.startService(Intent(context, PersistentService::class.java))
                }
            } else {
                Timber.i("Not starting Sotification because it's disabled")
            }
        }

        fun stopService(context: Context) {
            with(NotificationManagerCompat.from(context)) {
                cancel(FOREGROUND_ID)
            }
            context.stopService(Intent(context, PersistentService::class.java))
        }

        fun restartService(context: Context) {
            Timber.i("Restarting Sotification")
            stopService(context)
            startService(context)
        }

        private fun shouldStartForegroundService(context: Context): Boolean {
            return !context.isIgnoringBatteryOptimizations
        }
    }
}

class AutoStart : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (Prefs.isEnabled) {
            if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
                Timber.d("Received ACTION_BOOT_COMPLETED.")
                PersistentService.startService(context)
            } else if (intent.action == Intent.ACTION_MY_PACKAGE_REPLACED) {
                Timber.d("Received ACTION_MY_PACKAGE_REPLACED.")
                PersistentService.startService(context)
            }
        }
    }
}

const val PERSISTENT_NOTIFICATION_CHANNEL_ID = "persistent"

object NotificationHandler {

    @SuppressLint("StaticFieldLeak") // This is always used from the service, so the applicationContext is always available anyway
    val persistentNotification: NotificationCompat.Builder

    init {
        val applicationContext: Context = requireApplicationContext()

        @TargetApi(Build.VERSION_CODES.O)
        if (isOreoOrNewer) {
            val notificationChannel = NotificationChannel(
                PERSISTENT_NOTIFICATION_CHANNEL_ID,
                applicationContext.getString(R.string.notification_persistent),
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationChannel.description = applicationContext.getString(R.string.notification_persistent_channel_description)
            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val notificationIntent = Intent(applicationContext, MainActivity::class.java)
        notificationIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val notificationPendingIntent =
            PendingIntent.getActivity(applicationContext, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE)
        persistentNotification = NotificationCompat.Builder(applicationContext, "persistent").apply {
            color = ContextCompat.getColor(applicationContext, R.color.colorPrimary)
            setContentText(applicationContext.getString(R.string.notification_tap_to_open))
            setShowWhen(false)
            setContentIntent(notificationPendingIntent)
            setSmallIcon(R.drawable.ic_notification)
            setOngoing(true)
            setOnlyAlertOnce(true)
        }
    }

    fun showPersistentNotification(title: String, message: String) {

        persistentNotification.apply {
            setContentTitle(title)
            setContentText(message)
            setStyle(NotificationCompat.BigTextStyle().bigText(message))
        }

        with(NotificationManagerCompat.from(requireApplicationContext())) {
            Timber.d("Showing persistent notification with message '$message'")
            notify(PersistentService.FOREGROUND_ID, persistentNotification.build())
        }
    }
}
