package com.jarsilio.android.sotification

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import com.jarsilio.android.common.extensions.defaultEmail
import com.jarsilio.android.common.extensions.isOreoOrNewer
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.common.menu.CommonMenu
import com.jarsilio.android.common.privacypolicy.PrivacyPolicyBuilder
import com.jarsilio.android.sotification.appintro.AppIntro
import com.jarsilio.android.sotification.prefs.Prefs
import com.jarsilio.android.sotification.services.PERSISTENT_NOTIFICATION_CHANNEL_ID
import com.jarsilio.android.sotification.services.PersistentService
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.LibsBuilder
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private val prefs = Prefs
    private val commonMenu: CommonMenu by lazy { CommonMenu(this) }

    private lateinit var appIntroActivityResultLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportFragmentManager.beginTransaction().replace(R.id.settings, SettingsFragment()).commit()

        registerActivityResultLaunchers()
    }

    private fun registerActivityResultLaunchers() {
        appIntroActivityResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
            Timber.d("Returned from activity: requestCode ${activityResult.resultCode}. resultCode: ${activityResult.resultCode} (Activity.RESULT_OK = ${Activity.RESULT_OK})")
            if (activityResult.resultCode != Activity.RESULT_OK) {
                Timber.d("AppIntro didn't exit correctly (resultCode != Activity.RESULT_OK). Probably user went back. Closing MainActivity...")
                finish()
            }
        }
    }

    override fun onStart() {
        super.onStart()

        if (!prefs.appIntroShown) {
            Timber.e("AppIntro has never been shown. Opening it this one time...")
            appIntroActivityResultLauncher.launch(Intent(this, AppIntro::class.java))
            prefs.appIntroShown = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        commonMenu.addImpressumToMenu(menu)
        commonMenu.addSendDebugLogsToMenu(menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_privacy_policy -> showPrivacyPolicyActivity()
            R.id.menu_item_licenses -> showAboutLicensesActivity()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        invalidateOptionsMenu()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            delegate.localNightMode = prefs.dayNightMode
        }
    }

    private fun showPrivacyPolicyActivity() {
        val privacyPolicyBuilder = PrivacyPolicyBuilder()
            .withIntro(getString(R.string.app_name), "Juan García Basilio (juanitobananas)")
            .withUrl("https://gitlab.com/juanitobananas/sotification/blob/master/PRIVACY.md#sotification-privacy-policy")
            .withMeSection()
            .withEmailSection(defaultEmail)
            .withAutoGoogleOrFDroidSection()
        privacyPolicyBuilder.start(this)
    }

    private fun showAboutLicensesActivity() {
        var style = Libs.ActivityStyle.LIGHT_DARK_TOOLBAR
        var theme = R.style.AppTheme_About_Light

        val currentNightMode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            style = Libs.ActivityStyle.DARK
            theme = R.style.AppTheme_About_Dark
        }

        LibsBuilder()
            .withActivityStyle(style)
            .withActivityTheme(theme)
            .withAboutIconShown(true)
            .withAboutVersionShown(true)
            .withActivityTitle(getString(R.string.menu_item_licenses))
            .withAboutDescription(getString(R.string.licenses_about_libraries_text))
            .start(applicationContext)
    }

    class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

        private val prefs = Prefs
        private val progressBar: ProgressBar by lazy { requireActivity().findViewById(R.id.progressBar) }
        private val settingsLayout: FrameLayout by lazy { requireActivity().findViewById(R.id.settings) }

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            populateDayNightListPreference()
            // if (isOreoOrNewer) {
            if (false) {
                // Not showing the Advanced category (also not for more modern Androids). I am not sure this is really useful. I'll wait so see if people want to see this.
                findPreference<PreferenceCategory>(Prefs.ADVANCE_CATEGORY_V26)?.isVisible = true
            }
            bindClickListeners()
        }

        private fun bindClickListeners() {
            if (isOreoOrNewer) {
                findPreference<Preference>(Prefs.NOTIFICATION_V26)?.setOnPreferenceClickListener {
                    Timber.d("Opening system notification settings for Sotification.")

                    val intent =
                        Intent(android.provider.Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS).apply {
                            putExtra(
                                android.provider.Settings.EXTRA_APP_PACKAGE,
                                requireActivity().packageName
                            )
                            putExtra(
                                android.provider.Settings.EXTRA_CHANNEL_ID,
                                PERSISTENT_NOTIFICATION_CHANNEL_ID
                            )
                        }

                    startActivity(intent)
                    true
                }
            }
        }

        private fun populateDayNightListPreference() {
            val dayNightPreferences = findPreference<ListPreference>(Prefs.DAY_NIGHT_MODE)!!
            dayNightPreferences.entries = arrayOf(getString(R.string.settings_string_array_day), getString(R.string.settings_string_array_night))
            dayNightPreferences.entryValues = arrayOf(AppCompatDelegate.MODE_NIGHT_NO.toString(), AppCompatDelegate.MODE_NIGHT_YES.toString())
            dayNightPreferences.setDefaultValue(AppCompatDelegate.MODE_NIGHT_NO.toString())

            if (isPieOrNewer) {
                dayNightPreferences.entries = dayNightPreferences.entries.plus(getString(R.string.settings_string_array_follow_system))
                dayNightPreferences.entryValues = dayNightPreferences.entryValues.plus(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM.toString())
                dayNightPreferences.setDefaultValue(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM.toString())
            }
        }

        override fun onStart() {
            super.onStart()
            prefs.settingsFragment = this
        }

        override fun onResume() {
            super.onResume()
            prefs.prefs.registerOnSharedPreferenceChangeListener(this)
            settingsLayout.visibility = View.VISIBLE
            progressBar.visibility = View.INVISIBLE
        }

        override fun onPause() {
            super.onPause()
            prefs.prefs.unregisterOnSharedPreferenceChangeListener(this)
        }

        override fun onDestroy() {
            super.onDestroy()
            prefs.settingsFragment = null
        }

        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            Timber.d("Changed preference: $key")
            when (key) {
                prefs.IS_ENABLED -> {
                    if (prefs.isEnabled) {
                        PersistentService.startService(requireContext())
                    } else {
                        PersistentService.stopService(requireContext())
                    }
                }
                prefs.DAY_NIGHT_MODE -> {
                    Timber.d("Changing theme to ${prefs.dayNightMode}")
                    setNightMode(prefs.dayNightMode)
                }
            }
        }

        private fun setNightMode(nightMode: Int) {
            AppCompatDelegate.setDefaultNightMode(nightMode)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                (requireActivity() as AppCompatActivity).delegate.localNightMode = nightMode
            }
        }
    }
}
