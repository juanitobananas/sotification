package com.jarsilio.android.sotification.prefs

import android.annotation.SuppressLint
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.CheckBoxPreference
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreference
import androidx.preference.SwitchPreferenceCompat
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.sotification.MainActivity
import com.jarsilio.android.sotification.R
import com.jarsilio.android.sotification.requireApplicationContext

@SuppressLint("StaticFieldLeak") // It's the applicationContext and it's constantly being used in the service, so this shouldn't be a leak.
object Prefs {

    private val context = requireApplicationContext()

    val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    var settingsFragment: MainActivity.SettingsFragment? = null

    /* User prefs */
    val IS_ENABLED: String = context.getString(R.string.pref_enabled_key)
    val DAY_NIGHT_MODE: String = context.getString(R.string.pref_day_night_mode_key)

    /* Other "prefs" */
    val NOTIFICATION_V26 = context.getString(R.string.pref_notification_key)
    val ADVANCE_CATEGORY_V26 = context.getString(R.string.pref_advanced_category_key)

    val APP_INTRO_SHOWN = context.getString(R.string.pref_app_intro_shown_key)

    var isEnabled: Boolean
        get() = prefs.getBoolean(IS_ENABLED, true)
        set(value) = setBooleanPreference(IS_ENABLED, value)

    val dayNightMode: Int
        get() {
            val defaultDayNightMode = if (isPieOrNewer) {
                AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM.toString()
            } else {
                AppCompatDelegate.MODE_NIGHT_NO.toString()
            }
            return prefs.getString(DAY_NIGHT_MODE, defaultDayNightMode)!!.toInt()
        }

    var appIntroShown: Boolean
        get() = prefs.getBoolean(APP_INTRO_SHOWN, false)
        set(value) = setBooleanPreference(APP_INTRO_SHOWN, value)

    private fun setBooleanPreference(key: String, value: Boolean) {
        // This changes the GUI, but it needs the PreferencesActivity to have started
        when (val preference = settingsFragment?.findPreference(key) as Preference?) {
            is CheckBoxPreference -> preference.isChecked = value
            is SwitchPreference -> preference.isChecked = value
            is SwitchPreferenceCompat -> preference.isChecked = value
        }
        // This doesn't change the GUI
        prefs.edit().putBoolean(key, value).apply()
    }
}
