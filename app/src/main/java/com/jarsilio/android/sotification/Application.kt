package com.jarsilio.android.sotification

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import com.jarsilio.android.sotification.extensions.isInstalledViaGooglePlay
import com.jarsilio.android.sotification.prefs.Prefs
import org.acra.config.mailSender
import org.acra.config.notification
import org.acra.data.StringFormat
import org.acra.ktx.initAcra
import timber.log.Timber

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        appContext = applicationContext

        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))

        AppCompatDelegate.setDefaultNightMode(Prefs.dayNightMode)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        initAcra()
    }

    private fun initAcra() {
        if (!BuildConfig.DEBUG) {
            initAcra {
                buildConfigClass = BuildConfig::class.java
                reportFormat = StringFormat.JSON
                mailSender {
                    mailTo = if (isInstalledViaGooglePlay) {
                        "juam+autoautorotate-play@posteo.net"
                    } else {
                        "juam+autoautorotate@posteo.net"
                    }
                    reportFileName = "crash.txt"
                }
                notification {
                    title = getString(R.string.acra_notification_title)
                    text = getString(R.string.acra_notification_text)
                    channelName = getString(R.string.acra_notification_channel_name)
                    sendButtonText = getString(R.string.acra_notification_send)
                    discardButtonText = getString(android.R.string.cancel)
                    resSendButtonIcon = R.drawable.ic_email_gray
                    resDiscardButtonIcon = R.drawable.ic_cancel_gray
                }
            }
        }
    }
}

private var appContext: Context? = null

fun requireApplicationContext(): Context {
    return appContext ?: throw IllegalStateException("ApplicationContext should not be null. Did you forget to set it in your Application's onCreate()?")
}
