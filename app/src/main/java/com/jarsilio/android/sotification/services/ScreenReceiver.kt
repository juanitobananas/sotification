package com.jarsilio.android.sotification.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.preference.PreferenceManager
import com.jarsilio.android.sotification.R
import com.jarsilio.android.sotification.requireApplicationContext
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Timer
import java.util.TimerTask

object ScreenReceiver : BroadcastReceiver() {

    private var timer: Timer = Timer()

    private val prefs = PreferenceManager.getDefaultSharedPreferences(requireApplicationContext())

    override fun onReceive(context: Context, intent: Intent) {

        if (intent.action == Intent.ACTION_SCREEN_OFF) {
            Timber.d("Screen off. Disabling scheduled task to check running app (TimerTask)")
            unscheduleTimer()
            screenOnTimeToday += System.currentTimeMillis() - lastScreenOn
            Timber.d("Screen-on time (in seconds) today: ${screenOnTimeToday / 1000}")
        } else if (intent.action == Intent.ACTION_SCREEN_ON) {
            Timber.d("Screen on. Re-enabling scheduled task to check running app (TimerTask)")
            scheduleTimer()
            lastScreenOn = System.currentTimeMillis()
        }
    }

    fun unscheduleTimer() {
        timer.cancel()
    }

    fun scheduleTimer() {
        timer.cancel()
        timer = Timer()
        timer.schedule(
            object : TimerTask() {
                override fun run() {
                    Timber.d("Periodical notification on-screen time update (because screen is on)...")
                    NotificationHandler.showPersistentNotification("Screen-on time today", currentScreenOnTime)
                }
            },
            0, 60000
        )
    }

    var lastScreenOn: Long
        get() {
            return prefs.getLong("lastScreenOn", 0L)
        }
        set(value) {
            prefs.edit().putLong("lastScreenOn", value).apply()
        }

    private fun getScreenOnTime(date: String): Long {
        return prefs.getLong("${date}_screenOnTime", 0L)
    }

    private fun setScreenOnTime(date: String, screenOnTime: Long) {
        prefs.edit().putLong("${date}_screenOnTime", screenOnTime).apply()
    }

    private val currentScreenOnTime: String
        get() {
            return millisecondsToHoursAndMinutes(currentScreenOnTimeMillis)
        }

    private val currentScreenOnTimeMillis: Long
        get() {
            Timber.d("currentScreenOnTimeMillis: ${screenOnTimeToday + System.currentTimeMillis() - lastScreenOn}")
            return screenOnTimeToday + System.currentTimeMillis() - lastScreenOn
        }

    private fun millisecondsToHoursAndMinutes(timeInMilliseconds: Long): String {
        val timeInSeconds = timeInMilliseconds / 1000

        val hours = timeInSeconds / 3600
        val minutes = timeInSeconds / 60 % 60

        return when {
            hours == 0L && minutes == 0L -> requireApplicationContext().getString(R.string.time_less_than_a_minute)
            hours == 0L && minutes == 1L -> requireApplicationContext().getString(R.string.time_minute)
            hours == 0L && minutes > 1L -> requireApplicationContext().getString(R.string.time_minutes, minutes)
            hours == 1L && minutes == 1L -> requireApplicationContext().getString(R.string.time_hour_and_minute)
            hours == 1L && minutes != 1L -> requireApplicationContext().getString(R.string.time_hour_and_minutes, minutes)
            hours > 1L && minutes == 1L -> requireApplicationContext().getString(R.string.time_hours_and_minute, hours)
            else -> requireApplicationContext().getString(R.string.time_hours_and_minutes, hours, minutes)
        }
    }

    var screenOnTimeToday: Long
        get() {
            val today: String = SimpleDateFormat("yyyyMMdd").format(Date())
            return getScreenOnTime(today)
        }
        set(value) {
            val today: String = SimpleDateFormat("yyyyMMdd").format(Date())
            setScreenOnTime(today, value)
        }
}
