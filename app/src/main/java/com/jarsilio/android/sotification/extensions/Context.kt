package com.jarsilio.android.sotification.extensions

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.PowerManager

val Context.isScreenOn: Boolean
    get() {
        val powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            powerManager.isInteractive
        } else {
            powerManager.isScreenOn
        }
    }

val Context.isIgnoringBatteryOptimizations: Boolean
    get() {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val powerManager = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
            powerManager.isIgnoringBatteryOptimizations(applicationContext.packageName)
        } else {
            true
        }
    }

val Context.isInstalledViaGooglePlay: Boolean
    @TargetApi(Build.VERSION_CODES.R)
    get() {
        val installerPackage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            packageManager.getInstallSourceInfo(packageName).installingPackageName
        } else {
            packageManager.getInstallerPackageName(packageName)
        }

        return installerPackage == "com.android.vending"
    }
