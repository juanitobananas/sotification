# Sotification

Sotification is an Android app that shows the current screen-on time in
a notification.

<!--
## Download it

[<img src="https://gitlab.com/juanitobananas/wave-up/raw/master/f-droid/get-it-on.png"
      alt="Get it on IzzyOnDroid's F-Droid repo"
      height="80">](https://apt.izzysoft.de/app/com.jarsilio.android.sotification)

[<img src="https://gitlab.com/juanitobananas/wave-up/raw/master/google-play-store/google-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.jarsilio.android.sotification)

## Translations

Translations are always welcome! :)

The app is available for translation as two projects on Transifex: [auto-auto-rotate](https://www.transifex.com/juanitobananas/auto-auto-rotate/ "Auto Auto-Rotate on transifex")
and [libcommon](https://www.transifex.com/juanitobananas/libcommon/ "libcommon on transifex").

-->

## Donating

If you feel like donating, you can do so by using Liberapay [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/juanitobananas/).

Maybe bitcoin is more of your liking? Here you go: bc1qdl8md3dwmg39e9umyjfsdn9sy7mcpxfypen45a

<!--
## Legal Attributions

Google Play and the Google Play logo are trademarks of Google Inc.
-->
